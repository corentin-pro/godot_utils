class_name UDPCom


const PORT: int = 8080
const MAX_PACKET_SIZE = 65_000

var _udp_recv: PacketPeerUDP
var _udp_send: PacketPeerUDP
var pool_usec_delay = 0

var initialized: bool = false
signal com_init


func _init() -> void:
	_udp_recv = PacketPeerUDP.new()
	_udp_send = PacketPeerUDP.new()


func start(_data) -> void:
	print("COM : UDP connection started on port %d" % PORT)
	if _udp_recv.listen(PORT) == OK:
		initialized = true
		emit_signal("com_init")
	else:
		print("COM : Cannot listen to port %d" % PORT)


func stop() -> void:
	_udp_recv.close()
	_udp_send.close()
	initialized = false


func _receive_packet(timeout: float=-1) -> PoolByteArray:
	var start_time = null
	while _udp_recv.get_available_packet_count() == 0:
		if pool_usec_delay:
			OS.delay_usec(pool_usec_delay)
		if timeout >= 0:
			if start_time == null:
				start_time = OS.get_ticks_usec()
				continue
			elif float(OS.get_ticks_usec() - start_time) / 1000000.0 > timeout:
				return PoolByteArray([])
	return _udp_recv.get_packet()


func _ackownledge():
	var result = _receive_packet()
	return len(result) == 8 and bytes2var(result) == true


func _pack_response(response: ComMessage.Response) -> PoolByteArray:
	var message = response.to_dict()
	message = var2bytes(message)
	message = var2bytes(len(message)) + message
	return message


func _send_error(error_message: String):
	var _error = _udp_send.put_packet(_pack_response(
		ComMessage.Response.new(ComMessage.ERROR, error_message)))
	_error = _ackownledge()


func _send_response(response: ComMessage.Response) -> bool:
	var message = _pack_response(response)
	var message_len = len(message)
	var error

	if len(message) <= MAX_PACKET_SIZE:
		error = _udp_send.put_packet(message)
		if error != OK:
			_send_error("Godot error %d while sending" % [error])
			return false
		if not _ackownledge():
			return false
	else:
		var start_index: int = 0
		var last_index: int = message_len - 1
		while start_index + MAX_PACKET_SIZE < last_index:
			error = _udp_send.put_packet(message.subarray(
				start_index, start_index + MAX_PACKET_SIZE - 1))
			if error != OK:
				_send_error("Godot error %d while sending" % [error])
				return false
			if not _ackownledge():
				print("COM : ACK error")
				return false
			start_index += MAX_PACKET_SIZE
		if start_index < last_index:
			error = _udp_send.put_packet(message.subarray(
				start_index, last_index))
			if error != OK:
				_send_error("Godot error %d while sending" % [error])
				return false
			if not _ackownledge():
				print("COM : ACK error")
				return false
	return true


func receive(timeout: float=-1.0) -> ComMessage.Request:
	var received_message = _receive_packet(timeout)

	if len(received_message) == 0:
		print("COM : error : timeout")
		return null
	if len(received_message) <= 8:
		print("COM : error : packet too short (len <= 8)")
		return null
	if _udp_send.set_dest_address(_udp_recv.get_packet_ip(), PORT + 1) != OK:
		print("COM : cannot set destination address")
		return null

	var _error = _udp_send.put_var(true)
	var receive_len = bytes2var(received_message.subarray(0, 8))
	received_message = received_message.subarray(8, -1)
	var chunk = null
	while receive_len > len(received_message):
		chunk = _receive_packet(timeout)
		if len(chunk) == 0:
			return null
		received_message += chunk
		_error = _udp_send.put_var(true)
	return ComMessage.Request.new(bytes2var(received_message))


func send_message(message: ComMessage.Response) -> bool:
	return _send_response(message)
