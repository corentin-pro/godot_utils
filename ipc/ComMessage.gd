class_name ComMessage

const KEY_ACTION = "action"
const KEY_DATA = "data"
const KEY_STATUS = "status"

const OK = "ok"
const ERROR = "error"


class Request:
	var action: String
	var data

	func _init(p_data) -> void:
		if typeof(p_data) != TYPE_DICTIONARY:
			data = null
		else:
			if KEY_ACTION in p_data:
				action = p_data[KEY_ACTION]
			else:
				action = ""
			if KEY_DATA in p_data:
				data = p_data[KEY_DATA]
			else:
				data = null

	func to_dict() -> Dictionary:
		return {KEY_ACTION: self.action, KEY_DATA: self.data}

class Response:
	var status: String
	var data

	func _init(p_status: String, p_data):
		self.status = p_status
		self.data = p_data

	func to_dict() -> Dictionary:
		return {KEY_STATUS: self.status, KEY_DATA: self.data}
