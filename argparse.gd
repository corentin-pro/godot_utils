class_name Argparse

var should_exit: bool = false
var positional_arguments: PoolStringArray = PoolStringArray()
var positional_infos: Dictionary = {}
var optional_arguments: Dictionary = {
	PoolStringArray(["-h", "--help"]): {
		"default": null, "help": "show this help message and exit"},
}

func _assert(condition: bool, message):
	if not condition:
		print_usage()
		print("Argparse Error : ", message)
		should_exit = true

func print_usage() -> void:
	printraw("usage: %s [-h]" % ProjectSettings.get_setting(
		"application/config/name"))
	for arguments_name in optional_arguments:
		printraw(" [%s]" % [
			arguments_name if typeof(arguments_name) == TYPE_STRING
			else arguments_name.join(", ")])
	if not positional_arguments.empty():
		printraw(" ", positional_arguments.join(" "))
	print()

func print_help() -> void:
	print("\npositional arguments:")
	for arg in positional_arguments:
		print("  %s   %s" % [arg, positional_infos[arg].help])
	print("\noptional arguments:")
	for arguments_name in optional_arguments:
		print("  %s   %s" % [
			arguments_name if typeof(arguments_name) == TYPE_STRING
				else arguments_name.join(", "),
			optional_arguments[arguments_name].help])

func add_arguments(argument_name, help: String="", default=null, flag=false) -> void:
	assert(
		typeof(argument_name) == TYPE_STRING
		or typeof(argument_name) == TYPE_ARRAY,
		"argument_name must be a string or an array of strings")
	if typeof(argument_name) == TYPE_STRING:
		assert(argument_name != "", "argument name must not be empty")
		if argument_name[0] == "-":
			assert(
				!optional_arguments.has(argument_name),
				"optional argument %s already registered" % argument_name)
	
			optional_arguments[argument_name] = {
				"default": default, "help": help, "flag": flag}
		else:
			assert(
				!Array(positional_arguments).has(argument_name),
				"positional argument %s already registered" % argument_name)
			positional_arguments.append(argument_name)
			positional_infos[argument_name] = {
				"default": default, "help": help, "flag": flag}
	else:
		var args = PoolStringArray(argument_name)
		for arg in args:
			assert(arg[0] == "-", "optional argument must start with '-'")
			assert(arg != "", "argument name must not be empty")
			assert(
				!Array(positional_arguments).has(arg),
				"positional argument %s already registered" % arg)
		optional_arguments[args] = {
			"default": default, "help": help, "flag": flag}

func parse_args(args: PoolStringArray) -> Dictionary:
	var result: Dictionary = {}
	var argument_index: int = 0
	var argument: String
	var positional_index: int = 0
	
	# Special help arguments
	if "-h" in args or "--help" in args:
		print_usage()
		print_help()
		should_exit = true
		return {}

	while argument_index < args.size() and not should_exit:
		argument = args[argument_index]
		if argument[0] == '-':
			var key_found: bool = false
			for optional_key in optional_arguments:
				if typeof(optional_key) == TYPE_STRING_ARRAY:
					if Array(optional_key).has(argument):
						key_found = true
						argument = optional_key[-1]
				elif argument == optional_key:
					key_found = true
			_assert(
				key_found,
				"unrecognized arguments : %s" % argument)
			if not key_found:
				break
			if optional_arguments[argument].flag:
				result[argument] = true
			else:
				result[argument] = (
					args[argument_index + 1] if argument_index + 1 < args.size()
					else "")
				argument_index += 1
		else:
			_assert(
				positional_index < positional_arguments.size(),
				"too many positional arguments given")
		argument_index += 1

	if should_exit:
		return result

	# Checking for missing positional arguments
	var missing_args: Array = []
	for key in positional_arguments:
		if !result.keys().has(key):
			missing_args.append(key)
	_assert(
		missing_args.empty(),
		"the following arguments are required: %s" % PoolStringArray(
			missing_args).join(", "))

	if should_exit:
		return result

	# Assigning default values to non registered optional argument
	for key in optional_arguments:
		if (typeof(key) == TYPE_STRING
				and !result.keys().has(key)
				and optional_arguments[key].default != null):
			result[key] = true if optional_arguments[key].flag else optional_arguments[key].default

	return result
